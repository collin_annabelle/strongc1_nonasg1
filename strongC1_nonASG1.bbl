\begin{thebibliography}{1}

\bibitem{da2012isogeometric}
L.~Beirao~da Veiga, A.~Buffa, C.~Lovadina, M.~Martinelli, and G.~Sangalli.
\newblock An isogeometric method for the {Reissner--Mindlin} plate bending
  problem.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  209:45--53, 2012.

\bibitem{benson2011large}
D.~J. Benson, Y.~Bazilevs, M.-C. Hsu, and T.~JR Hughes.
\newblock A large deformation, rotation-free, isogeometric shell.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  200(13):1367--1378, 2011.

\bibitem{collin2015approximation}
A.~Collin, G.~Sangalli, and T.~Takacs.
\newblock Approximation properties of multi-patch {$C^1$} isogeometric spaces.
\newblock {\em arXiv preprint arXiv:1509.07619}, 2015.

\bibitem{gomez2008isogeometric}
H.~G{\'o}mez, V.~M Calo, Y.~Bazilevs, and T.~JR Hughes.
\newblock Isogeometric analysis of the {Cahn--Hilliard} phase-field model.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  197(49):4333--4352, 2008.

\bibitem{gomez2010isogeometric}
H.~G{\'o}mez, T.~JR Hughes, X.~Nogueira, and V.~M. Calo.
\newblock Isogeometric analysis of the isothermal {Navier--Stokes--Korteweg}
  equations.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  199(25):1828--1840, 2010.

\bibitem{kiendl-bletzinger-linhard-09}
J.~Kiendl, K.-U. Bletzinger, J.~Linhard, and R.~W{\"u}chner.
\newblock Isogeometric shell analysis with {K}irchhoff-{L}ove elements.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  198(49):3902--3914, 2009.

\end{thebibliography}
